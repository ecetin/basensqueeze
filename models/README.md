# Supported Models (11 models)

- BaseNSqueeze (8 models)
  - with CTC-decoder (xxs, xs, s)
    - with Dynamic Convolution and without Dynamic Convolution
  - with CRF-decoder (m)
    - with Dynamic Convolution and without Dynamic Convolution
- Bonito CRF model (default) (1 model)
- CATCaller (default) (1 model)
- SACall (default) (1 model)

## Details

Configuration files for each model is stored in `/config` folder. Each model has parameters stored in a `weights_*.tar` file in their respective folders and they have their optimizer parameters in `optim_*.tar` files. Training and evaluation results can be found in `results.log` and `evaluation.log` files, respectively.