# BaseNSqueeze

## 1 Abstract
Basecalling is a critical step in genome sequence analysis where nucleotide sequences are generated from the complex electrical signals. Different deep-learning models have been proposed for basecalling during the last years to provide high basecalling accuracy. The implementation of ASR models into basecalling is a common practice in designing highly-accurate basecallers.
In this work, we developed a basecalling method, BaseNSqueeze, by adapting a state-of-the-art ASR model, Squeezeformer, to the task of basecalling.
Our method has shown to achieve higher basecalling accuracy than prior methods with minor reduction in performance.

Next to implementing the BaseNSqueeze model, we adapted the SACall (https://github.com/huangnengCSU/SACall-basecaller) and CATCaller (https://github.com/biomed-AI/CATCaller/tree/master/train) code to the used framework in order to compare the different models. Training of all the used models is explained bellow.


## 2. Environment Setup
Download Training data
```
bonito --training
```

You can find the model configurations, pretrained model weights and optimizer state dictionaries in `models/` directory for all models that were trained from scratch.


## 3. Models
### 3.1 BaseNSqueeze
BaseNSqueeze: An end2end ONT Basecaller using Squeezeformer architecture.

#### 3.1.1 Requirements and Installation for BaseNSqueeze
* Python version == 3.8.15
* CUDA/11.3
* GCC/9.1.0
To install all required packages:
```
pip install -r requirements.txt
```

#### 3.1.2 Run training 
(also refer to `train_basensqueeze_ctc.sh` or `train_basensqueeze_crf.sh` in [bash_run](./bash_run) folder)
```
python trainer.py --training_directory models/basensqueeze_ctc_s \
--config models/configs/config_basensqueeze_ctc_s.toml \
--directory ../ETH/dna_r9.4.1/ --device cuda --lr 1e-4 --epochs 200 \
--batch 16 --chunks 100000 --valid_chunks 0 --val_freq 5 --no_amp \
-f --save_optim_every 5 --custom_models
```

```
python trainer.py --training_directory models/basensqueeze_crf_m \
--config models/configs/config_basensqueeze_crf_m.toml \
--directory ../dna_r9.4.1/ --device cuda --lr 1e-4 --epochs 200 \
--batch 16 --chunks 100000 --valid_chunks 0 --val_freq 5 --no_amp \
-f --save_optim_every 5 --custom_models
```

If you want to provide a pretrained model, simply add:
```
--pretrained models/basensqueeze_ctc_s
```
of course after changing the pretrained model name

### 3.2 [CATCaller](https://github.com/biomed-AI/CATCaller/tree/master/train) for Baseline

CATCaller: An end2end ONT Basecaller using Convolution-augmented Transformer.

#### 3.2.1 Requirements and Installation for CATCaller
* Python version >= 3.6 
* ninja package
* CUDA/10.0 (just for installation, then you can run at CUDA/11.3)
* GCC/7.3.0 (just for installation, then you can run at GCC/9.1.0)
* pytorch_gpu version >= 1.2.0    
* ont-bonito
* [dynamicConv/lightweightConv](https://github.com/pytorch/fairseq).
To install dynamicConv/lightweightConv module:
```
cd dynamicconv_layer
python cuda_function_gen.py
python setup.py build
python setup.py install --user
```

#### 3.2.2 Run training 
(also refer to `train_catcaller.sh` in [bash_run](./bash_run) folder)
```
python trainer.py --training_directory models/catcaller \
--config models/configs/config_catcaller.toml \
--directory ../ETH/dna_r9.4.1/ --device cuda --lr 1e-4 --epochs 200 \
--batch 16 --chunks 100000 --valid_chunks 0 --val_freq 5 --no_amp \
-f --save_optim_every 5 --custom_models
```

### 3.3 [SACall](https://github.com/huangnengCSU/SACall-basecaller) for Baseline

SACall: An end2end ONT Basecaller using a Transformer architecture.

#### 3.3.1 Requirements and Installation for SACall
* Python version >= 3.0
* pytorch_gpu version >= 1.0.1  
* ctcdecode: https://github.com/parlance/ctcdecode.git


#### 3.3.2 Run training 
(also refer to `train_sacaller.sh` in [bash_run](./bash_run) folder)
```
python trainer.py --training_directory models/sacaller \
--config models/configs/config_sacaller.toml \
--directory ../ETH/dna_r9.4.1/ --device cuda --lr 1e-4 --epochs 200 \
--batch 16 --chunks 100000 --valid_chunks 0 --val_freq 5 --no_amp \
-f --save_optim_every 5 --custom_models
```

command parameters  
`signal window length`: the length of the signal segment, default: `2048`.

## 4. Basecalling
To basecall raw signals using our pre-trained models. 
```
python basecall.py models/basensqueeze_ctc_s $signal_dir --batchsize $batchsize > $outfile
```

You can provide any trained model instead of basensqueeze_ctc_s for basecalling using the trained models.

## 5. Parameter Description
| Parameter          | Description                                                            |
|--------------------|------------------------------------------------------------------------|
| training_directory | Path to training directory where logs and weights will be saved        |
| config             | Path to config file (.toml format)                                     |
| pretrained         | Path to pretrained model                                               |
| directory          | Path to data directory (not a specific file, just directory)           |
| device             | Torch device (cpu, cuda)                                               |
| lr                 | Learning rate                                                          |
| seed               | Training seed                                                          |
| epochs             | Number of epochs                                                       |
| batch              | Batch size                                                             |
| chunks             | Number of chunks used for training                                     |
| valid_chunks       | Number of chunks used for validation                                   |
| val_freq           | Frequency of validation (by epochs)                                    |
| no_amp             | Use Automatic Mixed Precision package from torch (torch.amp)           |
| f &#124; force     | Force action on directory (i.e. overwrite existing training directory) |
| restore_optim      | Restore state of pretrained model                                      |
| nondeterministic   | Boolean to set training non-deterministic                              |
| save_optim_every   | Frequency of saving model state (by epochs)                            |
| grad_accum_split   | Number of splits for gradient loss                                     |
| custom_models      | Use custom model loader (used for BaseNSqueeze/CATCaller/SACall)       |

## 6. References
* [Squeezeformer Implementation by upskyy](https://github.com/upskyy/Squeezeformer)
```
@article{squeezeformer,
  url = {https://arxiv.org/abs/2206.00888},
  author = {Kim, Sehoon and Gholami, Amir and Shaw, Albert and Lee, Nicholas and Mangalam, Karttikeya and Malik, Jitendra and Mahoney, Michael W. and Keutzer, Kurt},
  title = {Squeezeformer: An Efficient Transformer for Automatic Speech Recognition},
  year = {2022},
}
```
* [Dynamic Convolution Implementation by kaijieshi7](https://github.com/kaijieshi7/Dynamic-convolution-Pytorch)
```
@article{dynamic_conv,
  url = {https://arxiv.org/abs/1912.03458},
  author = {Chen, Yinpeng and Dai, Xiyang and Liu, Mengchen and Chen, Dongdong and Yuan, Lu and Liu, Zicheng},
  title = {Dynamic Convolution: Attention over Convolution Kernels},
  year = {2019},
}
```
* [ONT-Bonito Repository](https://github.com/nanoporetech/bonito)
* [CATCAller Official Implementation](https://github.com/biomed-AI/CATCaller)
```
@article {lv2020endtoend,
  url = {https://www.biorxiv.org/content/early/2020/11/10/2020.11.09.374165},
  author = {Lv, Xuan and Chen, Zhiguang and Lu, Yutong and Yang, Yuedong},
  title = {An End-to-end Oxford Nanopore Basecaller Using Convolution-augmented Transformer},
  year = {2020},
  publisher = {Cold Spring Harbor Laboratory},
  eprint = {https://www.biorxiv.org/content/early/2020/11/10/2020.11.09.374165.full.pdf},
  journal = {bioRxiv}
}
```
* [SACall Official Implementation](https://github.com/huangnengCSU/SACall-basecaller)
```
@article{huang2022sacall,
  url={https://pubmed.ncbi.nlm.nih.gov/33211664/},
  author={Huang, Neng and Nie, Fan and Ni, Peng and Luo, Feng and Wang, Jianxin},
  journal={IEEE/ACM Transactions on Computational Biology and Bioinformatics},
  title={SACall: A Neural Network Basecaller for Oxford Nanopore Sequencing Data Based on Self-Attention Mechanism},
  year={2022},  
  volume={19},
  number={1},
  pages={614-623},
}
```
