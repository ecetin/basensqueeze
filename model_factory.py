#! /usr/bin/env python3

from basensqueeze_crf.model import Model as BaseNSqueeze_CRF
from basensqueeze_ctc.model import Squeezeformer as BaseNSqueeze_CTC
from catcaller.litetr_encoder import LiteTransformerEncoder_Catcall
from sacall.litetr_encoder import LiteTransformerEncoder_Sacall

class ModelFactory:
    def __init__():
        pass

    def get_model(config):
        model_name = config['model']['module']

        if model_name == 'basensqueeze_crf':
            return BaseNSqueeze_CRF(config)
        elif model_name == 'basensqueeze_ctc':
            return BaseNSqueeze_CTC(config)
        elif model_name == 'catcaller':
            return LiteTransformerEncoder_Catcall(config)
        elif model_name == 'sacaller':
            return LiteTransformerEncoder_Sacall(config)
        else:
            raise ModuleNotFoundError("Module {} was not found".format(model_name))

