# Reproducing the results in the report

We explain how we evaluate BaseNSqueeze in our report to enable easy reproduction of the results.

## Prerequisites

We evalutate the performance and accuracy benefits of BaseBSqueeze by integrating it into Bonito pipeline. We compare it to Bonito, SACall and CatCaller. We integrated SACall and CatCaller into our framework so they do not need to be installed seperately

We list the links to download and compile each tool for comparison below:

* [Bonito](https://github.com/nanoporetech/bonito)

We use various tools to process and analyze the data we generate using each tool. The following tools must also be installed in your machine:

* [Minimap2](https://github.com/lh3/minimap2)

Please make sure that all of these tools are in your `PATH`

## Datasets

All the datasets can be downloaded via the scripts we provide. In order to download references:

```bash
bash download_references.sh # Download references and organize it

```

In order to download the reads:

```bash
bash download_reads.sh # Download reads and organize it

```

Now that you have downloaded all the datasets, we can start running all the tools to collect the results.

## Generating Fasta Files

The first step in our evaluation is to use our models for basecalling the test read sets. Basecalling generates a file in the format known as fasta file which is used later for our accuracy analysis. To generate the fasta files for all reads and models we have (BaseNSqueeze, CatCaller, SACall), you can use the following script.

```bash
cd run_scripts
python run_basecall.py $read_dir $output_dir $model_dir 
cd ..
```

The paths must be absolute paths to the reads that are downloaded above, the main output directory under which you want to collect all your results, and the path to the models directory under this repository, such as;

```bash
cd run_scripts
python run_basecall.py /mnt/batty/bcavlak/courses/deeplearning/final_repo/basensqueeze/test/test_reads/ /mnt/batty/bcavlak/courses/deeplearning/final_repo/basensqueeze/test/data/ /mnt/batty/bcavlak/courses/deeplearning/final_repo/basensqueeze/models/
cd ..
```

For Bonito, you have to basecall with the following command:

```bash
cd run_scripts
python run_bonito.py $read_dir $output_dir $model_dir
cd ..
```
The $read_dir and $output_dir should be same as the above script. The model dir should be absolute path to [bonito-model directory](./bonito-model/).

Now that you have basecalled all reads, we can collect the accuracy and performance numbers.


## Accuracy Analysis

We calculate the accuracy with read idendity following prior approaches. Read identity measures the number of matching bases in read's alignment to the reference genome divided by the total alignment length including insertions and deletions, a.k.a. the ‘BLAST identity’. To calculate this, we first map the basecalled reads to their respective reference genomes. Reference genome is a representative DNA sequence for an organism. 

The paths must be absolute paths. The $refs_dir refers to the directory where the references downloaded with the download_references script. $fasta_dir should be equal to the $output_dir/fastq/ used above. $alignment_dir is the directory that will be used as the output directory for minimap (can be selected as $output_dir/sam/). 

```bash
cd run_scripts
python run_minimap.py $refs_dir $fasta_dir $alignment_dir 
cd ..
```

The next step is to analyze the alignment files to calculate the read identity. For that you can use the following script which computes read identities for all models and all species:

```bash
cd analysis
python sam_accuracy.py $alignment_dir
cd ..
```

The $alignment_dir should be the same as used in minimap script.

## Performance Analysis

We calculate the performance of each model with the samples per second metric. After basecalling using the all models, you can use the following commands to calculate the performance of all models for all species tested.

```bash
cd analysis
python performance.py $output_dir/runtime/ 
cd ..
```