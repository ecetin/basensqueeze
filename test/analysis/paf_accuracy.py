#!/usr/bin/env python3
"""
Copyright 2019 Ryan Wick (rrwick@gmail.com)
https://github.com/rrwick/Basecalling-comparison
This program is free software: you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. This program is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should
have received a copy of the GNU General Public License along with this program. If not, see
<http://www.gnu.org/licenses/>.
This script produces a table with information for each read.
Inputs:
  * Read file
  * minimap2 PAF
Output:
  * tsv file with these columns: length, identity, relative length
If less than half of a read aligns, it is deemed unaligned and given an identity of 0. If more than
half aligns, only the aligned parts are used to determined the read identity.
Relative length is included to see if the reads are systematically too short or too long.
"""


import collections
import gzip
import os
import statistics
import sys


def get_identity(read_filename, paf_filename):

    read_lengths = get_fasta_lengths(read_filename)
    read_names = sorted(read_lengths.keys())
    read_alignments = load_alignments(paf_filename, read_lengths)
    identities = []

    for read_num, read_name in enumerate(read_alignments.keys()):
        read_length = read_lengths[read_name]
        alignments = read_alignments[read_name]
        identity_by_base = [0.0] * read_length
        total_read_length, total_ref_length = 0, 0

        for i in range(alignments[0], alignments[1]):
            if alignments[-1] > identity_by_base[i]:
                identity_by_base[i] = alignments[-1]
        total_read_length = alignments[1] - alignments[0]
        total_ref_length = alignments[3] - alignments[2]

        # If less than half the read aligned, then we call it an unaligned read.
        whole_read_identity = 0.0
        if identity_by_base.count(0.0) > read_length / 2.0:
            whole_read_identity = 0.0
        # Otherwise, the read's identity is the average of the aligned parts.
        else:
            whole_read_identity = statistics.mean([x for x in identity_by_base if x > 0.0])

        identities.append(whole_read_identity)

    return sum(identities) / len(identities)


def print_progress(done_count, total_count):
    print('\r{:,} / {:,} reads complete'.format(done_count+1, total_count),
          end='', flush=True, file=sys.stderr)


def load_alignments(paf_filename, read_lengths):
    print('Loading alignments...', end='', flush=True, file=sys.stderr)
    read_alignments = {}
    with open(paf_filename, 'rt') as paf:
        for line in paf:
            paf_parts = line.strip().split('\t')
            if len(paf_parts) < 11:
                continue
            read_name = paf_parts[0]
            read_length = int(paf_parts[1])
            read_start = int(paf_parts[2])
            read_end = int(paf_parts[3])
            ref_start = int(paf_parts[7])
            ref_end = int(paf_parts[8])
            identity = 100.0 * int(paf_parts[9]) / int(paf_parts[10])
            if(read_name not in read_alignments.keys()):
                read_alignments[read_name] = (read_start, read_end, ref_start, ref_end, identity)
            elif read_alignments[read_name][-1] < identity:
                read_alignments[read_name] = (read_start, read_end, ref_start, ref_end, identity)

    print(' done', file=sys.stderr)
    return read_alignments


def get_fasta_lengths(fasta_filename):
    open_func = open
    read_lengths = {}
    with open_func(fasta_filename, 'rt') as fasta_file:
        name = ''
        sequence = []
        for line in fasta_file:
            line = line.strip()
            if not line:
                continue
            if line[0] == '@':  # Header line = start of new contig
                if name:
                    read_lengths[name.split()[0]] = len(''.join(sequence))
                    sequence = []
                name = line[1:]
            else:
                sequence.append(line)
        if name:
            read_lengths[name.split()[0]] = len(''.join(sequence))
    return read_lengths




read_dir_path = sys.argv[1]
paf_dir_path = sys.argv[2]


#models = ['basensqueeze_ctc_dyn_sm', 'basensqueeze_ctc_dyn_s', 'basensqueeze_ctc_dyn_xs', 'basensqueeze_ctc_dyn_xxs', 'basensqueeze_ctc_sm', 'basensqueeze_ctc_s', 'basensqueeze_ctc_xs', 'basensqueeze_ctc_xxs', 'catcaller', 'sacaller']
models = ['basensqueeze_ctc_s',  'basensqueeze_ctc_xs',  'basensqueeze_ctc_xxs']

identities = []

for model in models:
    identities.append([])
    dir_list = os.listdir(paf_dir_path + model)
    for dir in dir_list:
        test = dir.strip().split(".")
        paf_path = paf_dir_path + model + '/' + dir
        fasta_path = read_dir_path + model + '/' + test[0] + '.fasta'
        identity = get_identity(fasta_path, paf_path)
        identities[-1].append(identity)

print(identities)

for model in models:
    print("\t" + model, end = "")

dir_list = os.listdir(sam_dir_path + models[0])
for idx, species in enumerate(dir_list):
    species = species.strip().split('.')[0]
    print("\n" + species, end = "")
    for idx2, model in enumerate(models):
        print("\t" + str(identities[idx2][idx]), end = "")
