import collections
import os
import statistics
import sys


def calculate_identity(sam_filename):
    read_alignments = load_alignments(sam_filename)
    return sum(read_alignments.values()) / len(read_alignments)


def load_alignments(sam_filename):
    print('Loading alignments...', end='', flush=True, file=sys.stderr)
    read_alignments = {}
    with open(sam_filename, 'rt') as sam:
        for line in sam:
            if not line.startswith('@'):
                sam_parts = line.strip().split('\t')
                read_name = sam_parts[0]
                cigar = sam_parts[5]
                read_length = len(sam_parts[9])
                cigar = cigar.replace('I', 'I ')
                cigar = cigar.replace('D', 'D ')
                cigar = cigar.replace('X', 'X ')
                cigar = cigar.replace('=', '= ')
                cigar = cigar.replace('S', 'S ')
                cigar = cigar.replace('N', 'N ')
                cigar = cigar.replace('H', 'H ') 
                cigar = cigar.replace('P', 'P ') 
                cigar_parts = cigar.strip().split(' ')
                match_length = 0
                clip_length = 0
                for element in cigar_parts:
                    if element[-1] == '=':
                        match_length = match_length + int(element[:-1])
                    elif element[-1] == 'S':
                        clip_length = clip_length + int(element[:-1])
                identity = 100.0 * int(match_length) / int(read_length - clip_length)
                if(read_name not in read_alignments.keys()):
                    read_alignments[read_name] = identity
                elif read_alignments[read_name] < identity:
                    read_alignments[read_name] = identity
    print(' done', file=sys.stderr)
    return read_alignments


sam_dir_path = sys.argv[1]


models = ['basensqueeze_ctc_dyn_s', 'basensqueeze_ctc_dyn_xs', 'basensqueeze_ctc_dyn_xxs', 'basensqueeze_ctc_s', 'basensqueeze_ctc_xs', 'basensqueeze_ctc_xxs', 'catcaller', 'sacall', 'bonito']
#models = ['basensqueeze_ctc_s',  'basensqueeze_ctc_xs',  'basensqueeze_ctc_xxs']

identities = []

for model in models:
    identities.append([])
    dir_list = os.listdir(sam_dir_path + model)
    for dir in dir_list:
        test = dir.strip().split(".")
        sam_path = sam_dir_path + model + '/' + test[0] + ".sam"
        identity = calculate_identity(sam_path)
        identities[-1].append(identity)

print(identities)

for model in models:
    print("\t" + model, end = "")

dir_list = os.listdir(sam_dir_path + models[0])
for idx, species in enumerate(dir_list):
    print("\n" + species, end = "")
    for idx2, model in enumerate(models):
        print("\t" + str(identities[idx2][idx]), end = "")
