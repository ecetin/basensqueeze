import sys
import os

runtime_dir_path = sys.argv[1]


models = ['basensqueeze_ctc_dyn_s', 'basensqueeze_ctc_dyn_xs', 'basensqueeze_ctc_dyn_xxs', 'basensqueeze_ctc_s', 'basensqueeze_ctc_xs', 'basensqueeze_ctc_xxs', 'catcaller', 'sacaller-rtx', 'bonito']
#models = ['basensqueeze_ctc_s',  'basensqueeze_ctc_xs',  'basensqueeze_ctc_xxs']


runtimes = []

for model in models:
    runtimes.append([])
    dir_list = os.listdir(runtime_dir_path + model)
    for dir in dir_list:
        file_path = runtime_dir_path + model + '/' + dir
        with open(file_path, 'rt') as f:
            for line in f:
                if line.startswith('> samples'):
                    runtimes[-1].append(line.strip().split(' ')[-1])        

print(runtimes)

for model in models:
    print("\t" + model, end = "")

dir_list = os.listdir(runtime_dir_path + models[0])
for idx, species in enumerate(dir_list):
    print("\n" + species, end = "")
    for idx2, model in enumerate(models):
        print("\t" + str(runtimes[idx2][idx]), end = "")
