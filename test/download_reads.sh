#!/bin/bash

mkdir -p reads
mkdir -p reads/Acinetobacter_pittii_16-377-0801
wget -O reads/Acinetobacter_pittii_16-377-0801_fast5s.tar.gz  https://bridges.monash.edu/ndownloader/files/14260511; tar -xzf reads/Acinetobacter_pittii_16-377-0801_fast5s.tar.gz -C reads/Acinetobacter_pittii_16-377-0801; rm reads/Acinetobacter_pittii_16-377-0801_fast5s.tar.gz
mkdir -p reads/Klebsiella_pneumoniae_INF032
wget -O reads/Klebsiella_pneumoniae_INF032_fast5s.tar.gz https://bridges.monash.edu/ndownloader/files/15188573; tar -xzf reads/Klebsiella_pneumoniae_INF032_fast5s.tar.gz -C reads/Klebsiella_pneumoniae_INF032; rm reads/Klebsiella_pneumoniae_INF032_fast5s.tar.gz
mkdir -p reads/Staphylococcus_aureus_CAS38_02
wget -O reads/Staphylococcus_aureus_CAS38_02_fast5s.tar.gz https://bridges.monash.edu/ndownloader/files/14260568; tar -xzf reads/Staphylococcus_aureus_CAS38_02_fast5s.tar.gz -C reads/Staphylococcus_aureus_CAS38_02; rm reads/Staphylococcus_aureus_CAS38_02_fast5s.tar.gz
