#!/bin/bash

mkdir -p references
wget -O references/Acinetobacter_pittii_16-377-0801_reference.fasta.gz https://bridges.monash.edu/ndownloader/files/14260217; gunzip references/Acinetobacter_pittii_16-377-0801_reference.fasta.gz
wget -O references/Klebsiella_pneumoniae_INF032_reference.fasta.gz https://bridges.monash.edu/ndownloader/files/14260223; gunzip references/Klebsiella_pneumoniae_INF032_reference.fasta.gz
wget -O references/Serratia_marcescens_17-147-1671_reference.fasta.gz  https://bridges.monash.edu/ndownloader/files/14260235; gunzip references/Serratia_marcescens_17-147-1671_reference.fasta.gz
wget -O references/Staphylococcus_aureus_CAS38_02_reference.fasta.gz https://bridges.monash.edu/ndownloader/files/14260241; gunzip references/Staphylococcus_aureus_CAS38_02_reference.fasta.gz
