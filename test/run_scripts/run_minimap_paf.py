import os
import sys 

refs_path = sys.argv[1]
fasta_dir_path = sys.argv[2]
paf_dir_path = sys.argv[3]

#models = ['basensqueeze_ctc_dyn_sm', 'basensqueeze_ctc_dyn_s', 'basensqueeze_ctc_dyn_xs', 'basensqueeze_ctc_dyn_xxs', 'basensqueeze_ctc_sm', 'basensqueeze_ctc_s', 'basensqueeze_ctc_xs', 'basensqueeze_ctc_xxs', 'catcaller', 'sacaller']
models = ['basensqueeze_ctc_s',  'basensqueeze_ctc_xs',  'basensqueeze_ctc_xxs']

os.system("mkdir -p " + paf_dir_path)

for model in models:
    dir_list = os.listdir(fasta_dir_path + model)
    for dir in dir_list:
        test = dir.strip().split(".")
        ref_path = refs_path + test[0] + '_reference.fasta '
        fasta_path = fasta_dir_path + model + '/' + dir
        paf_path = paf_dir_path + model + '/' + test[0] + ".paf"
        os.system("mkdir -p " + paf_dir_path + model)
        print("\nbio minimap2 -x map-ont --secondary=no -t 40 " + ref_path + " " + fasta_path + " > " + paf_path)
        os.system("minimap2 -x map-ont --secondary=no -t 40 " + ref_path + " " + fasta_path + " > " + paf_path)
