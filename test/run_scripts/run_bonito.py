import os
import sys 

read_dir_path = sys.argv[1]
output_dir_path = sys.argv[2]
model_path = sys.argv[3]

dir_list = os.listdir(read_dir_path)
batch_size = 64

for dir in dir_list:
    fastq_dir = output_dir_path + 'fastq/bonito/'
    runtim_dir = output_dir_path + 'runtime/bonito/'
    os.system("mkdir -p " + fastq_dir)
    os.system("mkdir -p " + runtim_dir)
    print('\n bonito basecaller ' + model_path + ' ' + read_dir_path + dir + '/ --batchsize ' + str(batch_size) + ' > ' + fastq_dir + dir +  '.fastq 2> ' + runtim_dir + dir + '.out')
    os.system('bonito basecaller ' + model_path + ' ' + read_dir_path + dir + '/ --batchsize ' + str(batch_size) + ' > ' + fastq_dir + dir +  '.fastq 2> ' + runtim_dir + dir + '.out')
    os.system("rm " + fastq_dir + dir + "_summary.tsv")
    os.system("python ../utils/fastq_to_fasta.py " + fastq_dir + dir + '.fastq ' +  fastq_dir + dir + '.fasta')
        
        

