import os
import sys 

read_dir_path = sys.argv[1]
output_dir_path = sys.argv[2]
models_dir = sys.argv[3]
models = ['basensqueeze_ctc_dyn_s', 'basensqueeze_ctc_dyn_xs', 'basensqueeze_ctc_dyn_xxs', 'basensqueeze_ctc_s', 'basensqueeze_ctc_xs', 'basensqueeze_ctc_xxs', 'catcaller', 'sacaller']
#models = ['sacall', 'catcaller']

dir_list = os.listdir(read_dir_path)
batch_size = 64

for model in models:
    for dir in dir_list:
        fastq_dir = output_dir_path + 'fastq/' + model + '/'
        runtim_dir = output_dir_path + 'runtime/' + model + '/'
        os.system("mkdir -p " + fastq_dir)
        os.system("mkdir -p " + runtim_dir)
        print('\npython ../../basecall.py ' + models_dir + model + '/ ' + read_dir_path + dir + '/ --batchsize ' + str(batch_size) + ' > ' + fastq_dir + dir +  '.fastq 2> ' + runtim_dir + dir + '.out')
        os.system('python ../../basecall.py ' + models_dir + model + '/ ' + read_dir_path + dir + '/ --batchsize ' + str(batch_size) + ' > ' +  fastq_dir + dir + '.fastq 2> ' + runtim_dir + dir + '.out')
        os.system("rm " + fastq_dir + dir + "_summary.tsv")
        os.system("python ../utils/fastq_to_fasta.py " + fastq_dir + dir + '.fastq ' +  fastq_dir + dir + '.fasta')
        
