import os
import sys 

refs_path = sys.argv[1]
fasta_dir_path = sys.argv[2]
sam_dir_path = sys.argv[3]

models = ['basensqueeze_ctc_dyn_s', 'basensqueeze_ctc_dyn_xs', 'basensqueeze_ctc_dyn_xxs', 'basensqueeze_ctc_s', 'basensqueeze_ctc_xs', 'basensqueeze_ctc_xxs', 'catcaller', 'sacall', 'bonito']
#models = ['basensqueeze_ctc_s',  'basensqueeze_ctc_xs',  'basensqueeze_ctc_xxs']

os.system("mkdir -p " + sam_dir_path)

for model in models:
    dir_list = os.listdir(fasta_dir_path + model)
    for dir in dir_list:
        test = dir.strip().split(".")
        ref_path = refs_path + test[0] + '_reference.fasta '
        fasta_path = fasta_dir_path + model + '/' + dir
        sam_path = sam_dir_path + model + '/' + test[0] + ".sam"
        os.system("mkdir -p " + sam_dir_path + model)
        print("\nminimap2 --eqx -a -x map-ont --secondary=no -t 40 " + ref_path + " " + fasta_path + " > " + sam_path)
        os.system("minimap2 --eqx -a -x map-ont --secondary=no -t 40 " + ref_path + " " + fasta_path + " > " + sam_path)
