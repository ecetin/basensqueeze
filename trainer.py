#!/usr/bin/env python3

"""
Bonito training.
"""

import os
from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter
from pathlib import Path
from importlib import import_module
import glob

from bonito.data import load_numpy, load_script
from bonito.util import __models__, default_config, default_data
from bonito.util import load_model, load_symbol, init, half_supported
from bonito.training import load_state

import toml
import torch
import numpy as np
from torch.utils.data import DataLoader

from model_factory import ModelFactory
from cat_sa_caller_trainer import Cat_Sa_CallerTrainer
from basensqueeze_trainer import SqueezeTrainer

import logging

def get_last_checkpoint(dirname):
    weight_files = glob(os.path.join(dirname, "weights_*.tar"))
    if not weight_files:
        raise FileNotFoundError("no model weights found in '%s'" % dirname)
    weights = max([int(re.sub(".*_([0-9]+).tar", "\\1", w)) for w in weight_files])
    return os.path.join(dirname, 'weights_%s.tar' % weights)

def set_config_defaults(config, chunksize=None, batchsize=None, overlap=None, quantize=False):
    basecall_params = config.get("basecaller", {})
    # use `value or dict.get(key)` rather than `dict.get(key, value)` to make
    # flags override values in config
    basecall_params["chunksize"] = chunksize or basecall_params.get("chunksize", 4000)
    basecall_params["overlap"] = overlap if overlap is not None else basecall_params.get("overlap", 500)
    basecall_params["batchsize"] = batchsize or basecall_params.get("batchsize", 64)
    basecall_params["quantize"] = basecall_params.get("quantize") if quantize is None else quantize
    config["basecaller"] = basecall_params
    return config

def load_model(dirname, device, weights=None, half=None, chunksize=None, batchsize=None, overlap=None, quantize=False, use_koi=False):
    """
    Load a model config and weights off disk from `dirname`.
    """
    if not os.path.isdir(dirname) and os.path.isdir(os.path.join(__models__, dirname)):
        dirname = os.path.join(__models__, dirname)
    weights = get_last_checkpoint(dirname) if weights is None else os.path.join(dirname, 'weights_%s.tar' % weights)
    config = toml.load(os.path.join(dirname, 'config.toml'))
    config = set_config_defaults(config, chunksize, batchsize, overlap, quantize)
    return _load_model(weights, config, device, half, use_koi)

def main(args):

    workdir = os.path.expanduser(args.training_directory)

    if os.path.exists(workdir) and not args.force:
        print("[error] %s exists, use -f to force continue training." % workdir)
        exit(1)
    elif not os.path.exists(workdir):
        os.makedirs(workdir, exist_ok=True)

    logging.basicConfig(filename=args.training_directory + "/results.log", level=logging.INFO)
    init(args.seed, args.device, (not args.nondeterministic))
    device = torch.device(args.device)

    if not args.pretrained:
        config = toml.load(args.config)
    else:
        if args.custom_models:
            config = toml.load(glob.glob(args.pretrained + "/*.toml")[0])
        else:
            dirname = args.pretrained
            if not os.path.isdir(dirname) and os.path.isdir(os.path.join(__models__, dirname)):
                dirname = os.path.join(__models__, dirname)
            pretrain_file = os.path.join(dirname, 'config.toml')
            config = toml.load(pretrain_file)
            if 'lr_scheduler' in config:
                logging.info(f"[ignoring 'lr_scheduler' in --pretrained config]")
                del config['lr_scheduler']
        
        if 'lr_scheduler' in config:
            logging.info(f"[ignoring 'lr_scheduler' in --pretrained config]")
            del config['lr_scheduler']

    argsdict = dict(training=vars(args))

    logging.info("[loading model]")
    if args.custom_models:
        model = ModelFactory.get_model(config)
        if args.pretrained:
            pret_file = glob.glob(args.pretrained + "/weights_*.tar")[-1]
            logging.info(f"[using pretrained model {pret_file}]")
            message = model.load_state_dict(torch.load(pret_file))
            logging.info(str(message))
    else:
        if args.pretrained:
            logging.info(f"[using pretrained model {args.pretrained}]")
            model = load_model(args.pretrained, device, half=False)
            model.load_state_dict(torch.load(args.pretrained))
        else:
            model = load_symbol(config, 'Model')(config)

    logging.info("[loading data]")
    try:
        train_loader_kwargs, valid_loader_kwargs = load_numpy(
            args.chunks, args.directory
        )
    except FileNotFoundError:
        train_loader_kwargs, valid_loader_kwargs = load_script(
            args.directory,
            seed=args.seed,
            chunks=args.chunks,
            valid_chunks=args.valid_chunks,
            n_pre_context_bases=getattr(model, "n_pre_context_bases", 0),
            n_post_context_bases=getattr(model, "n_post_context_bases", 0),
        )

    loader_kwargs = {
        "batch_size": args.batch, "num_workers": 4, "pin_memory": True
    }
    train_loader = DataLoader(**loader_kwargs, **train_loader_kwargs)
    valid_loader = DataLoader(**loader_kwargs, **valid_loader_kwargs)

    os.makedirs(workdir, exist_ok=True)

    if config.get("lr_scheduler"):
        sched_config = config["lr_scheduler"]
        lr_scheduler_fn = getattr(
            import_module(sched_config["package"]), sched_config["symbol"]
        )(**sched_config)
    else:
        lr_scheduler_fn = None

    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    num_params = sum([np.prod(p.size()) for p in model_parameters])

    logging.info(f"BaseNSqueeze has {num_params} parameters")
    argsdict["num_params"] = num_params
    toml.dump({**config, **argsdict}, open(os.path.join(workdir, 'config.toml'), 'w'))
    
    if config.get("model").get("module") == "sacaller" or config.get("model").get("module") == "catcaller":
        trainer = Cat_Sa_CallerTrainer(
            model, device, train_loader, valid_loader,
            use_amp=half_supported() and not args.no_amp,
            lr_scheduler_fn=lr_scheduler_fn,
            restore_optim=args.restore_optim,
            save_optim_every=args.save_optim_every,
            grad_accum_split=args.grad_accum_split
        )
    else:
        trainer = SqueezeTrainer(
            model, device, train_loader, valid_loader,
            use_amp=half_supported() and not args.no_amp,
            lr_scheduler_fn=lr_scheduler_fn,
            restore_optim=args.restore_optim,
            save_optim_every=args.save_optim_every,
            grad_accum_split=args.grad_accum_split
        )

    if (',' in args.lr):
        lr = [float(x) for x in args.lr.split(',')]
    else:
        lr = float(args.lr)
    trainer.fit(workdir, args.epochs, lr, args.val_freq, pretrained=args.pretrained)

def argparser():
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("--training_directory", help="Path to training directory where logs and weights will be saved")
    parser.add_argument('--config', default=default_config, help="Path to config file (.toml format)")
    parser.add_argument('--pretrained', default="", help="Path to pretrained model")
    parser.add_argument("--directory", type=Path, help="Path to data directory (not a specific file, just directory)")
    parser.add_argument("--device", default="cuda", help="Torch device (cpu, cuda)")
    parser.add_argument("--lr", default='2e-3', help="Learning rate")
    parser.add_argument("--seed", default=25, type=int, help="Training seed")
    parser.add_argument("--epochs", default=5, type=int, help="Number of epochs")
    parser.add_argument("--batch", default=64, type=int, help="Batch size")
    parser.add_argument("--chunks", default=0, type=int, help="Number of chunks used for training")
    parser.add_argument("--valid_chunks", default=0, type=int, help="Number of chunks used for validation")
    parser.add_argument("--val_freq", default=5, type=int, help="Frequency of validation (by epochs)")
    parser.add_argument("--no_amp", action="store_true", default=False, help="Use Automatic Mixed Precision package from torch (torch.amp)")
    parser.add_argument("-f", "--force", action="store_true", default=False, help="Force action on directory (i.e. overwrite existing training directory)")
    parser.add_argument("--restore_optim", action="store_true", default=False, help="Restore state of pretrained model")
    parser.add_argument("--nondeterministic", action="store_true", default=False, help="Boolean to set training non-deterministic")
    parser.add_argument("--save_optim_every", default=10, type=int, help="Frequency of saving model state (by epochs)")
    parser.add_argument("--grad_accum_split", default=1, type=int, help="Number of splits for gradient loss")
    parser.add_argument('--custom_models', action="store_true", default=False, help="Use custom model loader (used for BaseNSqueeze/CATCaller/SACall)")
    return parser


if __name__ == "__main__":
    args = argparser().parse_args()
    main(args)

