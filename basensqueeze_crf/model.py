
import numpy as np
import torch
import torch.nn as nn
from torch.nn.functional import log_softmax, ctc_loss

from basensqueeze_crf.feature_encoder import FeatureEncoder
from squeezeformer.encoder import SqueezeformerEncoder
from bonito.crf.model import SeqdistModel, CTC_CRF

from fast_ctc_decode import beam_search, viterbi_search


class BaseNSqueeze(nn.Module):

    def __init__(self, config):
        super().__init__()
        self.feature_encoder = FeatureEncoder(**config["feature_encoder"])
        self.encoder = SqueezeformerEncoder(**config["encoder"])
    def forward(self, x, input_lengths):
        out = self.feature_encoder(x)
        out = torch.permute(out, (0, 2, 1))
        encoder_outputs, encoder_output_lengths = self.encoder(out, input_lengths)
        encoder_outputs = torch.permute(encoder_outputs, (1,0,2))
        return encoder_outputs
        

class Model(SeqdistModel):
    def __init__(self, config):
        seqdist = CTC_CRF(
            state_len=config['global_norm']['state_len'],
            alphabet=config['labels']['labels']
        )
        encoder = BaseNSqueeze(config)
        super().__init__(encoder, seqdist)
        self.config = config
    
    def forward(self, x, lengths):
        output = self.encoder(x, lengths)
        return log_softmax(output, dim=-1)
