
from typing import List, Tuple
import torch
import torch.nn as nn


class FeatureEncoder(nn.Module):
    """
    Feature encoder which consists of feature encoder blocks that encodes 1D temporal data using convolution.
    """

    def __init__(self, conv_layers: List[List[int]], bias: bool = False, repeat: int = 1):
        super(FeatureEncoder, self).__init__()
        
        self.blocks = nn.ModuleList()
        in_channels = 1
        for conv in conv_layers:
            self.blocks.append(
                FeatureEncoderBlock(
                    in_channels=in_channels,
                    conv=tuple(conv),
                    bias=bias,
                    repeat=repeat
                )
            )
            in_channels = conv[0]

    def forward(self, x):
        # x = x.unsqueeze(1)
        for block in self.blocks:
            x = block(x)
            
        return x


class FeatureEncoderBlock(nn.Module):
    """
    Feature encoder block which consists of a 1D convolution, 1d pooling, batch normalization and a GELU activation.
    """

    def __init__(self, in_channels: int, conv: Tuple[int, int, int], bias: bool = False, repeat: int = 1):
        super(FeatureEncoderBlock, self).__init__()
        
        out_channels, kernel_size, stride = conv
        self.block = nn.ModuleList()
        for _ in range(repeat):
            self.block.append(
                nn.Conv1d(
                    in_channels=in_channels,
                    out_channels=out_channels,
                    kernel_size=kernel_size,
                    stride=1,
                    padding=kernel_size // 2,
                    bias=bias
                )
            )
            in_channels = out_channels
        if stride > 1:
            self.block.append(
                nn.MaxPool1d(
                    kernel_size=stride,
                    stride=stride
                )
            )
        self.block.extend(
            [
                nn.BatchNorm1d(out_channels),
                nn.GELU()
            ]
        )

    def forward(self, x):
        for layer in self.block:
            x = layer(x)
        return x