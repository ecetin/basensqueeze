"""
Bonito model evaluator
"""

import os
import time
import torch
import numpy as np
from itertools import starmap
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from pathlib import Path

from bonito.data import load_numpy, load_script, ChunkDataSet
from bonito.util import accuracy, poa, decode_ref, half_supported
from bonito.util import init, load_model, concat, permute

from torch.utils.data import DataLoader

import sys
sys.path.insert(0, "../")

from catcaller.litetr_encoder import LiteTransformerEncoder_Catcall as Model
import catcaller.generate_dataset.constants as constants

import glob
import toml
import logging


def main(args):

    logging.basicConfig(filename=args.model_directory + "/evaluation.log", level=logging.INFO)

    poas = []
    init(args.seed, args.device)

    logging.info("* loading data")
    try:
        last_k_chunks = 100000
        test_loader_kwargs, _ = load_numpy(args.chunks, args.directory)
        test_loader_kwargs["dataset"] = ChunkDataSet(
            chunks=test_loader_kwargs["dataset"].chunks[-last_k_chunks:][:, 0, :],
            targets=test_loader_kwargs["dataset"].targets[-last_k_chunks:],
            lengths=test_loader_kwargs["dataset"].lengths[-last_k_chunks:]
        )
    except FileNotFoundError:
        test_loader_kwargs, _ = load_script(
            args.directory,
            seed=args.seed,
            chunks=args.chunks,
            valid_chunks=args.chunks
        )

    dataloader = DataLoader(
        batch_size=args.batchsize, num_workers=2, pin_memory=True,
        **test_loader_kwargs
    )

    accuracy_with_cov = lambda ref, seq: accuracy(ref, seq, min_coverage=args.min_coverage)

    seqs = []

    logging.info("* loading model")
    config = toml.load(glob.glob(args.model_directory + "/*.toml")[-1])

    model = Model(config).float().to(args.device)
    pret_file = glob.glob(args.model_directory + f"/weights_e{args.weights}.tar")[-1]
    logging.info(f"Loading from: {pret_file}")
    message = model.load_state_dict(torch.load(pret_file))

    logging.info(str(message))
    
    logging.info("* calling")

    targets = []

    t0 = time.perf_counter()
    with torch.no_grad():
        for idx, (data, target, length) in enumerate(dataloader):
            targets.extend(torch.unbind(target, 0))
            data = data.permute((0, 2, 1))
            data = data.type(torch.float32).to(args.device)
            target = target.type(torch.LongTensor).to(args.device)
            
            length = data.squeeze(
                2).ne(constants.SIG_PAD).sum(1)
            
            enc_output, _ = model(data, length)
            log_probs = enc_output.transpose(
                    1, 0).log_softmax(2)
            log_probs = log_probs.transpose(1, 0)

            seqs.extend([model.decode(p) for p in log_probs])

    duration = time.perf_counter() - t0

    refs = [decode_ref(target, model.alphabet) for target in targets]
    accuracies = [accuracy_with_cov(ref, seq) if len(seq) else 0. for ref, seq in zip(refs, seqs)]

    time_dim = data.shape[1]

    logging.info("* mean      %.2f%%" % np.mean(accuracies))
    logging.info("* median    %.2f%%" % np.median(accuracies))
    logging.info("* time      %.2f" % duration)
    logging.info("* samples/s %.2E" % (args.chunks * time_dim / duration))

    for i, (ref, seq) in enumerate(zip(refs, seqs)):
        logging.info(f"Predicted seq    {i:>7d}: {seq}")
        logging.info(f"Ground truth seq {i:>7d}: {ref}")

    if args.poa:

        logging.info("* doing poa")
        t0 = time.perf_counter()
        # group each sequence prediction per model together
        poas = [list(seq) for seq in zip(*poas)]
        consensuses = poa(poas)
        duration = time.perf_counter() - t0
        accuracies = list(starmap(accuracy_with_coverage_filter, zip(references, consensuses)))

        logging.info("* mean      %.2f%%" % np.mean(accuracies))
        logging.info("* median    %.2f%%" % np.median(accuracies))
        logging.info("* time      %.2f" % duration)


def argparser():
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        add_help=False
    )
    parser.add_argument("--model_directory")
    parser.add_argument("--directory", type=Path)
    parser.add_argument("--device", default="cuda")
    parser.add_argument("--seed", default=9, type=int)
    parser.add_argument("--weights", default="0", type=str)
    parser.add_argument("--chunks", default=1000, type=int)
    parser.add_argument("--batchsize", default=96, type=int)
    parser.add_argument("--beamsize", default=5, type=int)
    parser.add_argument("--poa", action="store_true", default=False)
    parser.add_argument("--min-coverage", default=0.5, type=float)
    return parser


if __name__ == "__main__":
    args = argparser().parse_args()

    main(args)