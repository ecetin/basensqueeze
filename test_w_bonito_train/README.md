
# Testing on Bonito's dna_r9.4.1 Dataset
For testing we pick the dna chunks in range [900K, 1M] as those chunks are never seen by our models. Our training scheme included training and evaluating on chunks that were in the range of [0, 100K] where we split the training and validation data.

Evaluation results can be found in `./models/($model_name)/evaluation.log`.

## Testing parameters
`model_directory`: BLA-BLA
`decoder`:
`directory`:
`device`:
`seed`:
`weights`:
`chunks`:
`batchsize`:
`beamsize`:
`poa`:
`min-coverage`:

# 1. BaseNSqueeze

BaseNSqueeze: An end2end ONT Basecaller using Squeezeformer architecture.

#### Run evaluation
```
python evaluate_basensqueeze.py --model_directory --decoder crf ../models/basensqueeze_crf_dyn_m \
--directory ../../../dna_r9.4.1/ --weights 22 --chunks 1000000 --batchsize 32 --beamsize 5
```
```
python evaluate_basensqueeze.py --model_directory --decoder crf ../models/basensqueeze_crf_m \
--directory ../../../dna_r9.4.1/ --weights 26 --chunks 1000000 --batchsize 32 --beamsize 5
```
```
python evaluate_basensqueeze.py --model_directory ../models/basensqueeze_ctc_xxs \
--directory ../../ETH/dna_r9.4.1 --weights 85 --chunks 1000000 --batchsize 32 --beamsize 5
```
```
python evaluate_basensqueeze.py --model_directory ../models/basensqueeze_ctc_xs \
--directory ../../../dna_r9.4.1/ --weights 150 --chunks 1000000 --batchsize 32 --beamsize 5
```
```
python evaluate_basensqueeze.py --model_directory ../models/basensqueeze_ctc_s \
--directory ../../../dna_r9.4.1/ --weights 110 --chunks 1000000 --batchsize 32 --beamsize 5
```
```
python evaluate_basensqueeze.py --model_directory ../models/basensqueeze_ctc_dyn_xxs \
--directory ../../../dna_r9.4.1/ --weights 30 --chunks 1000000 --batchsize 32 --beamsize 5
```
```
python evaluate_basensqueeze.py --model_directory ../models/basensqueeze_ctc_dyn_xs \
--directory ../../../dna_r9.4.1/ --weights 135 --chunks 1000000 --batchsize 32 --beamsize 5
```
```
python evaluate_basensqueeze.py --model_directory ../models/basensqueeze_ctc_dyn_s \
--directory ../../../dna_r9.4.1/ --weights 95 --chunks 1000000 --batchsize 32 --beamsize 5
```

# 2. [CATCaller](https://github.com/biomed-AI/CATCaller/tree/master/train) for Baseline

CATCaller: An end2end ONT Basecaller using Convolution-augmented Transformer.

#### Run evaluation
```
python evaluate_catcaller.py --model_directory ../models/catcaller/ \
--directory ../../../dna_r9.4.1/ --weights 170 --chunks 1000000 --batchsize 32 --beamsize 3
```

# 3. [SACall](https://github.com/huangnengCSU/SACall-basecaller) for Baseline

SACaller: An end2end ONT Basecaller using a Transformer architecture.

#### Run evaluation
```
python evaluate_sacall.py --model_directory ../models/sacall/ \
--directory ../../../dna_r9.4.1/ --weights 200 --chunks 1000000 --batchsize 32 --beamsize 3
```

# 4. [Bonito](https://github.com/nanoporetech/bonito) for Baseline

A PyTorch Basecaller for Oxford Nanopore Reads.

#### Run evaluation
```
python evaluate_bonito.py --model_directory ../models/bonito-model \
--decoder crf --directory ../../../dna_r9.4.1/ --weights 8
```