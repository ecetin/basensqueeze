"""
Bonito model evaluator
"""

import os
import time
import torch
import numpy as np
from itertools import starmap
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from pathlib import Path

from bonito.data import load_numpy, load_script, ChunkDataSet
from bonito.util import accuracy, poa, decode_ref, half_supported
from bonito.util import init, load_model, concat, permute

from torch.utils.data import DataLoader

import sys
sys.path.insert(0, "../")

import glob
import toml
import logging


def main(args):

    logging.basicConfig(filename=args.model_directory + "/evaluation.log", level=logging.INFO)

    poas = []
    init(args.seed, args.device)

    logging.info("* loading data")
    try:
        last_k_chunks = 100000
        train_loader_kwargs, valid_loader_kwargs = load_numpy(args.chunks, args.directory)
        train_loader_kwargs["dataset"] = ChunkDataSet(
            chunks=train_loader_kwargs["dataset"].chunks[-last_k_chunks:][:, 0, :],
            targets=train_loader_kwargs["dataset"].targets[-last_k_chunks:],
            lengths=train_loader_kwargs["dataset"].lengths[-last_k_chunks:]
        )
    except FileNotFoundError:
        train_loader_kwargs, valid_loader_kwargs = load_script(
            args.directory,
            seed=args.seed,
            chunks=args.chunks,
            valid_chunks=args.chunks
        )
    
    dataloader = DataLoader(
        batch_size=args.batchsize, num_workers=2, pin_memory=True,
        **train_loader_kwargs
    )

    accuracy_with_cov = lambda ref, seq: accuracy(ref, seq, min_coverage=args.min_coverage)

    seqs = []

    logging.info("* loading model")
    # config = toml.load(glob.glob(args.model_directory + "/*.toml")[-1])
    pret_file = glob.glob(args.model_directory + f"/weights_{args.weights}.tar")[-1]
    logging.info(f"Loading from: {pret_file}")
    model = load_model(args.model_directory, args.device, half=False)

    logging.info("* calling")

    targets = []

    t0 = time.perf_counter()
    with torch.no_grad():
        for idx, (data, target, length) in enumerate(dataloader):
            targets.extend(torch.unbind(target, 0))
            scores = model(data.to(args.device))
            
            if hasattr(model, 'decode_batch'):
                seqs.extend(model.decode_batch(scores))
            else:
                seqs.extend([model.decode(p) for p in permute(scores, 'TNC', 'NTC')])

    duration = time.perf_counter() - t0
    time_dim = data.shape[2]

    refs = [decode_ref(target, model.alphabet) for target in targets]
    accuracies = [accuracy_with_cov(ref, seq) if len(seq) else 0. for ref, seq in zip(refs, seqs)]

    if args.poa: poas.append(sequences)

    logging.info("* mean      %.2f%%" % np.mean(accuracies))
    logging.info("* median    %.2f%%" % np.median(accuracies))
    logging.info("* time      %.2f" % duration)
    logging.info("* samples/s %.2E" % (args.chunks * time_dim / duration))

    for i, (ref, seq) in enumerate(zip(refs, seqs)):
        logging.info(f"Predicted seq    {i:>7d}: {seq}")
        logging.info(f"Ground truth seq {i:>7d}: {ref}")

    if args.poa:

        print("* doing poa")
        t0 = time.perf_counter()
        # group each sequence prediction per model together
        poas = [list(seq) for seq in zip(*poas)]
        consensuses = poa(poas)
        duration = time.perf_counter() - t0
        accuracies = list(starmap(accuracy_with_coverage_filter, zip(references, consensuses)))

        print("* mean      %.2f%%" % np.mean(accuracies))
        print("* median    %.2f%%" % np.median(accuracies))
        print("* time      %.2f" % duration)


def argparser():
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        add_help=False
    )
    parser.add_argument("--model_directory")
    parser.add_argument("--decoder", default="ctc", type=str)
    parser.add_argument("--directory", type=Path)
    parser.add_argument("--device", default="cuda")
    parser.add_argument("--seed", default=9, type=int)
    parser.add_argument("--weights", default="0", type=str)
    parser.add_argument("--chunks", default=1000000, type=int)
    parser.add_argument("--batchsize", default=32, type=int)
    parser.add_argument("--beamsize", default=5, type=int)
    parser.add_argument("--poa", action="store_true", default=False)
    parser.add_argument("--min-coverage", default=0.5, type=float)
    return parser


if __name__ == "__main__":
    args = argparser().parse_args()

    if args.decoder == "ctc":
        from basensqueeze_ctc import Squeezeformer as Model
    elif args.decoder == "crf":
        from basensqueeze_crf import Model as Model
    else:
        raise(f"Decoder {args.decoder} does not exist.")
        exit(1)

    main(args)