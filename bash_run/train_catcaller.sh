#!/bin/bash
# 
# CompecTA (c) 2017
#
# You should only work under the /scratch/users/<username> directory.
#
# Example job submission script
#
# TODO:
#   - Set name of the job below changing "Test" value.
#   - Set the requested number of tasks (cpu cores) with --ntasks parameter.
#   - Select the partition (queue) you want to run the job in:
#     - short : For jobs that have maximum run time of 120 mins. Has higher priority.
#     - long  : For jobs that have maximum run time of 7 days. Lower priority than short.
#     - longer: For testing purposes, queue has 31 days limit but only 3 nodes.
#   - Set the required time limit for the job with --time parameter.
#     - Acceptable time formats include "minutes", "minutes:seconds", "hours:minutes:seconds", "days-hours", "days-hours:minutes" and "days-hours:minutes:seconds"
#   - Put this script and all the input file under the same directory.
#   - Set the required parameters, input and output file names below.
#   - If you do not want mail please remove the line that has --mail-type
#   - Put this script and all the input file under the same directory.
#   - Submit this file using:
#      sbatch examle_submit.sh

# -= Resources =-
#
#SBATCH --job-name=catcall                   # DON'T FORGET TO UPDATE
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=ai
#SBATCH --qos=ai
#SBATCH --account=ai
#SBATCH --exclude=be[01-12],da[01-04]
#SBATCH --mem-per-cpu=8000                # Deneme için comment outladım, 11.07.21

#####For the Dataloader with multiple workers########
#SBATCH --cpus-per-task=4                 # Yanlış sanırım uğur ntasks-per kullanıyor DATALOADER için
#####################################################

#SBATCH --constraint="tesla_t4"
##SBATCH --constraint="tesla_v100|tesla_t4"
#SBATCH --mem=30000
#SBATCH --gres=gpu:1

#SBATCH --time=7-0:0:0                # DON'T FORGET TO UPDATE
#SBATCH --output=comp-%j.out          # DON'T FORGET TO UPDATE
#SBATCH --error=comp-%j.err           # DON'T FORGET TO UPDATE
#SBATCH --mail-type=ALL
#SBATCH --mail-user=ecetin17@ku.edu.tr

################################################################################
##################### !!! DO NOT EDIT BELOW THIS LINE !!! ######################
################################################################################

echo "Setting stack size to unlimited..."
ulimit -s unlimited
ulimit -l unlimited
ulimit -a
echo

## Load Modules

module load anaconda/3.6
module load cuda/11.3
module load gcc/9.1.0

echo "==============================================================================="
source activate basecall
nvidia-smi

python trainer.py --training_directory models/catcaller \
--config models/configs/config_catcaller.toml \
--directory ../ETH/dna_r9.4.1/ --device cuda --lr 1e-4 --epochs 200 \
--batch 16 --chunks 100000 --valid_chunks 0 --val_freq 5 --no_amp \
-f --save_optim_every 5 --custom_models

source deactivate basecall
