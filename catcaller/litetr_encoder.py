
import torch.nn as nn
import torch
import numpy as np

from catcaller.modules import PositionalEncoding, EncoderLayer

from fast_ctc_decode import beam_search, viterbi_search
import Levenshtein as Lev
from torch.nn.functional import ctc_loss


class LiteTransformerEncoder_Catcall(nn.Module):
    def __init__(self, config):
        super(LiteTransformerEncoder_Catcall, self).__init__()

        d_model = config["encoder"]["d_model"]
        d_ff = config["encoder"]["d_ff"]
        n_head = config["encoder"]["n_head"] 
        num_encoder_layers = config["encoder"]["n_layers"]
        dropout = config["encoder"]["dropout"]

        label_vocab_size = config["decoder"]["label_vocab_size"]
        
        # EREN ADDED
        self.alphabet = config["labels"]["labels"]
        self.qscale = config["qscore"]["scale"]
        self.qbias = config["qscore"]["bias"]
        ##############################

        self.padding = 1
        self.kernel_size = 3
        self.stride = 2
        self.dilation = 1

        self.src_embed = nn.Sequential(nn.Conv1d(in_channels=config["input"]["features"],
                                                 out_channels=d_model//2,
                                                 kernel_size=self.kernel_size,
                                                 stride=self.stride,
                                                 padding=self.padding,
                                                 dilation=self.dilation,
                                                 bias=False),
                                       nn.BatchNorm1d(num_features=d_model//2),
                                       nn.ReLU(inplace=True),

                                       nn.Conv1d(in_channels=d_model//2,
                                                 out_channels=d_model,
                                                 kernel_size=self.kernel_size,
                                                 stride=self.stride,
                                                 padding=self.padding,
                                                 dilation=self.dilation,
                                                 bias=False),
                                       nn.BatchNorm1d(num_features=d_model),
                                       nn.ReLU(inplace=True),
                                       )
        # TODO: why padding_idx=0
        self.position_encoding = PositionalEncoding(
            d_model=d_model, dropout=dropout)

        self.stack_layers = nn.ModuleList(
            [EncoderLayer(index=i, d_model=d_model, d_ff=d_ff, n_head=n_head, dropout=dropout) for i in range(
                num_encoder_layers)])   #need change
        self.layer_norm = nn.LayerNorm(d_model, eps=1e-6)
        self.final_proj = nn.Linear(d_model, label_vocab_size)

    def forward(self, signal, signal_lengths):
        """
        :param signal: a tensor shape of [batch, length, 1]
        :param signal_lengths: a tensor shape of [batch,]
        :return:
        """

        max_len = signal.size(1)

        max_len = int(((max_len + 2 * self.padding - self.dilation * (
                self.kernel_size - 1) - 1) / self.stride + 1))

        max_len = int(((max_len + 2 * self.padding - self.dilation * (
                self.kernel_size - 1) - 1) / self.stride + 1))

        new_signal_lengths = ((signal_lengths + 2 * self.padding - self.dilation * (
                self.kernel_size - 1) - 1) / self.stride + 1).int()

        new_signal_lengths = ((new_signal_lengths + 2 * self.padding - self.dilation * (
                self.kernel_size - 1) - 1) / self.stride + 1).int()

        src_mask = torch.tensor([[0] * v.item() + [1] * (max_len - v.item()) for v in new_signal_lengths],
                                dtype=torch.uint8).unsqueeze(-2).to(signal.device) #[N,1,L]need change

        signal = signal.transpose(-1, -2)  # (N,C,L)


        embed_out = self.src_embed(signal)  # (N,C,L)

        embed_out = embed_out.transpose(-1, -2)  # (N,L,C)
        enc_output = self.position_encoding(embed_out)

        for layer in self.stack_layers:
            enc_output, enc_slf_attn = layer(enc_output, src_mask)

        enc_output = self.layer_norm(enc_output)

        enc_output = self.final_proj(enc_output)

        return enc_output, new_signal_lengths


    # EREN ADDED
    def decode(self, x, beamsize=3, threshold=1e-3, qscores=False, return_path=False):
        x = x.exp().cpu().numpy().astype(np.float32)
        if beamsize == 1 or qscores:
            seq, path  = viterbi_search(x, self.alphabet, qscores, self.qscale, self.qbias)
        else:
            seq, path = beam_search(x, self.alphabet, beamsize, threshold)
        if return_path: return seq, path
        return seq

    # EREN ADDED
    def cer(self, s1, s2):
        """
        Computes the Character Error Rate, defined as the edit distance.
        Arguments:
            s1 (string): space-separated sentence
            s2 (string): space-separated sentence
        """
        s1, s2, = s1.replace(' ', ''), s2.replace(' ', '')
        return Lev.distance(s1, s2)

    def ctc_label_smoothing_loss(self, log_probs, targets, lengths, weights=None):
        T, N, C = log_probs.shape
        weights = weights or torch.cat([torch.tensor([0.4]), (0.1 / (C - 1)) * torch.ones(C - 1)])
        log_probs_lengths = torch.full(size=(N, ), fill_value=T, dtype=torch.int64)
        loss = ctc_loss(log_probs.to(torch.float32), targets, log_probs_lengths, lengths, reduction='mean')
        label_smoothing_loss = -((log_probs * weights.to(log_probs.device)).mean())
        return {'total_loss': loss + label_smoothing_loss, 'loss': loss, 'label_smooth_loss': label_smoothing_loss}

    def loss(self, log_probs, targets, lengths):
        return self.ctc_label_smoothing_loss(log_probs, targets, lengths)
